package com.geekhub;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

public class ImageTask implements Runnable {

    private URL url;
    private String folder;

    public ImageTask(URL url, String folder) {
        this.url = url;
        this.folder = folder;
    }

    @Override
    public void run() {
        try (InputStream inputStream = url.openStream()) {
            FileOutputStream outputStream = new FileOutputStream(folder + "\\" + buildFileName(url), true);
            byte[] buffer = ConnectionUtils.getData(url);
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }

        } catch (Exception e) {

        }
    }

    private String buildFileName(URL url) {
        return url.toString().replaceAll("[^a-zA-Z0-9-_\\.]", "_");
    }
}
