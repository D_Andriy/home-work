package com.geekhub;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class ConnectionUtils {

    public static byte[] getData(URL url) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte[] imageBytes = new byte[4096];
        int bytesRead;
        InputStream stream = url.openStream();

        while ((bytesRead = stream.read(imageBytes)) > 0) {
            outputStream.write(imageBytes, 0, bytesRead);
        }
        stream.close();

        return outputStream.toByteArray();
    }
}
