package com.geekhub;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Page {

    private static final Pattern imageLinkPattern = Pattern.compile("<img.*?src=\"(.*?)\".*?(/>|</img>)");

    private String content;
    private URL url;

    public Page(URL url) throws IOException {
        StringBuilder builderContent = new StringBuilder();
        URLConnection urlConnection = url.openConnection();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            builderContent.append(line).append("\n");
        }
        bufferedReader.close();

        this.content = builderContent.toString();
        this.url = url;
    }

    public Collection<URL> getImageLinks() throws MalformedURLException {
        return extractMatches(imageLinkPattern.matcher(content));
    }

    private Collection<URL> extractMatches(Matcher matcher) throws MalformedURLException {
        List<URL> result = new ArrayList<>();
        while (matcher.find()) {
            try {
                if (matcher.toString().startsWith("http")) {
                    result.add(new URL(matcher.group(1)));
                } else {
                    result.add(new URL("http://" + url.getHost() + matcher.group(1)));
                }
            } catch (Exception e) {

            }
        }
        return result;
    }
}
