package com.geekhub;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageCrawler {

    public static final int NUMBER_OF_THREADS = 10;

    private ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private String folder;

    public ImageCrawler(String folder) throws MalformedURLException {
        this.folder = folder;
    }

    public void downloadImages(String urlToPage) throws IOException {
        Page page = new Page(new URL(urlToPage));
        Collection<URL> imageLinks = page.getImageLinks();
        for (URL u : imageLinks) {
            if (isImageURL(u)) {
                executorService.submit(new ImageTask(u, folder));
            }
        }
    }

    public void stop() {
        executorService.shutdown();
    }

    private boolean isImageURL(URL url) {
        return url.toString().matches("([^\\s]+(\\.(?i)(jpg|png))$)");
    }
}
