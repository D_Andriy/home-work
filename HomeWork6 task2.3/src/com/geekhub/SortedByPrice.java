package com.geekhub;

import java.util.Comparator;

class SortedByPrice implements Comparator <Car> {

    @Override
    public int compare(Car o1, Car o2) {
        char indexByName1 = o1.getName().charAt(2);
        char indexByName2 = o2.getName().charAt(2);

        if (indexByName1 == indexByName2) {
            return -1;
        }else {
            return 0;
        }
    }
}
