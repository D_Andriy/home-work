package com.geekhub;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        List <Car> cars = new ArrayList<>();
        cars.add(new Car("BMW", 30500));
        cars.add(new Car("Mercedes-Benz", 50000));
        cars.add(new Car("Ford", 28600));
        cars.add(new Car("Mercedes-Benz", 28000));

        Collections.sort(cars, new SortedByName());
        Collections.sort(cars, new SortedByPrice());

        cars.forEach(e -> System.out.println(e.getName() + " - " + e.getPrice()));


    }
}
