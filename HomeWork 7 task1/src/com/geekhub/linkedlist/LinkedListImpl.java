package com.geekhub.linkedlist;

import java.util.Iterator;

public class LinkedListImpl<E> implements LinkedList<E> {

    private int size = 0;

    private Node<E> head;
    private Node<E> tail;



    @Override
    public void add(E element) {
        Node<E> node = new Node<>(element, null);
        if (head == null) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            tail = node;
        }
        size++;
    }

    @Override
    public E get(int index) {
        if (index < 0 || index > size()) throw new IndexOutOfBoundsException();

        if (index == 0) {
            return head.value;
        }

        Node<E> node = head.next;
        for (int i = 1; i < index; i++) {
            node = node.next;
        }

        return node.value;
    }

    @Override
    public boolean contains(E element) {
        Node<E> node = head;
        while (node != null) {
            if (node.value.equals(element)) {
                return true;
            }
            node = node.next;
        }

        return false;
    }

    @Override
    public boolean delete(E element) {
        Node<E> previous = null;
        Node<E> node = head;

        while (node != null) {
            if (node.value.equals(element)) {
                if (previous != null) {
                    previous.next = node.next;
                    if (node.next == null) {
                        tail = previous;
                    }
                } else {
                    head = head.next;
                    if (head == null) {
                        tail = null;
                    }
                }
                size--;

                return true;
            }
            previous = node;
            node = node.next;
        }

        return false;
    }

    @Override
    public E delete(int index) {
        if (index < 0 || index > size()) throw new IndexOutOfBoundsException();

        E element = get(index);
        Node<E> node = head;

        if (index == 0) {
            head = head.next;
        }

        for (int i = 0; i < index-1; i++) {
            node = node.next;
        }
        node.next = node.next.next;

        size--;

        return element;

    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int clean() {
        int count = size;
        head = null;
        tail = null;
        size = 0;

        return count;
    }


    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl<>(head);
    }

    private static class IteratorImpl<E> implements Iterator<E> {

        private Node<E> node;

        public IteratorImpl(Node<E> node) {
            this.node = node;
        }

        @Override
        public boolean hasNext() {
            return node != null;
        }

        @Override
        public E next() {
            E value = node.value;
            node = node.next;
            return value;
        }
    }
}
