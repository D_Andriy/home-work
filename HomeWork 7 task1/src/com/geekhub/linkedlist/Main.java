package com.geekhub.linkedlist;

public class Main {

    public static void main(String[] args) {
        LinkedList<Element> linkedList = new LinkedListImpl<>();

        linkedList.add(new Element(0, "O"));
        linkedList.add(new Element(1, "A"));
        linkedList.add(new Element(2, "B"));
        linkedList.add(new Element(3, "C"));
        linkedList.add(new Element(4, "D"));
        linkedList.add(new Element(5, "E"));
        linkedList.add(new Element(6, "F"));
        linkedList.add(new Element(7, "G"));

        linkedList.forEach(System.out::println);
        System.out.println("get " + linkedList.get(7));
        System.out.println("contains " + linkedList.contains(new Element(2, "B")));
        System.out.println("size " + linkedList.size());
        System.out.println("delete index " + linkedList.delete(5));
        linkedList.forEach(System.out::println);
        System.out.println("size " + linkedList.size());
        System.out.println("delete E " + linkedList.delete(new Element(6, "F")));
        System.out.println("is empty " + linkedList.isEmpty());
        System.out.println("size " + linkedList.size());
        linkedList.forEach(System.out::println);
        linkedList.clean();
        System.out.println("is empty " + linkedList.isEmpty());
    }
}