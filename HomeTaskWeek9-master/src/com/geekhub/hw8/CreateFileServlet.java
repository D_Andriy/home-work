package com.geekhub.hw8;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@WebServlet("/file/create")
public class CreateFileServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathParameter = req.getParameter("path");
        String fileParameter = req.getParameter("file");
        String pathDecode = URLDecoder.decode(pathParameter, StandardCharsets.UTF_8.name());
        String fileDecode = URLDecoder.decode(fileParameter, StandardCharsets.UTF_8.name());
        Path path = Paths.get(pathDecode + "\\" + fileDecode);
        if(!Files.exists(path)){
            Files.createFile(path);
        }

        resp.sendRedirect("/dir/view");
    }
}
