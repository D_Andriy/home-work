package com.geekhub.hw8;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(value = "/dir/view", initParams = {
        @WebInitParam(name = "root", value = "D:\\")
})
public class ViewDirectoryServlet extends HttpServlet {

    private static Path ROOT_PATH;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ROOT_PATH = Paths.get(config.getInitParameter("root"));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Path path;
        String pathParameter = req.getParameter("path");
        if (pathParameter != null) {
            String pathDecode = URLDecoder.decode(pathParameter, StandardCharsets.UTF_8.name());
            path = Paths.get(pathDecode);
        } else {
            path = ROOT_PATH;
        }

        resp.setContentType("text/html;charset=UTF-8");

        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>");
        sb.append("<script> \n" +
                "$(document).ready(function(){\n" +
                "    $(\"#flip\").click(function(){\n" +
                "        $(\"#panel\").slideDown(\"slow\");\n" +
                "    });\n" +
                "});\n" +
                "</script>");
        sb.append("<body link=\"black\" vlink=\"black\" alink=\"black\">");
        sb.append("<style>");
        sb.append("td {\n" +
                "     background: red;\n" +
                "     padding: 5xp;\n" +
                "}");
        sb.append("#panel, #flip {\n" +
                "    padding: 5px;\n" +
                "    text-align: center;\n" +
                "    background-color: grey;\n" +
                "    border: solid 1px #c3c3c3;\n" +
                "}\n" +
                "\n" +
                "#panel {\n" +
                "    padding: 50px;\n" +
                "    display: none;\n" +
                "}");
        sb.append("</style>");
        sb.append("<body>");
        sb.append("<table align=\"center\">");
        appendLink(sb, path);
        sb.append("</table>");
        sb.append("</body>");
        sb.append("</html>");
        resp.getWriter().write(sb.toString());
    }

    private void appendLink(StringBuilder sb, Path path) throws IOException {
        List<Path> pathList = Files.walk(path, 1)
                .collect(Collectors.toList());
        for (Path p : pathList) {
            String pathEncoding = URLEncoder.encode(p.toString(), StandardCharsets.UTF_8.name());
            if (Files.isDirectory(p)) {
                sb.append("<tr>");
                sb.append("<th> <a href=\"/dir/view?path=" + pathEncoding + "\">" + p.getFileName() + "</a> </th>");
                sb.append("</tr>");
            } else {
                sb.append("<tr>");
                sb.append("<th> <a href=\"/file/view?path=" + pathEncoding + "\">" + p.getFileName() + "</a> </th>");
                sb.append("<td> <a href=\"/file/remove?file=" + pathEncoding + "\"> -remove</a> </td>");
                sb.append("</tr>");
            }
        }
        sb.append("<div id=\"flip\">Click to create file</div>");
        sb.append("<div id=\"panel\">");
        sb.append("<form action=\"/file/create\">");
        sb.append("Create file in directory <input type=\"text\" name=\"path\" value=\"" + path + "\">");
        sb.append("with file name <input type=\"text\" name=\"file\" value=\"name.txt\">");
        sb.append("<input type=\"submit\" value=\"create file\">");
        sb.append("</form>");
        sb.append("</div>");
    }
}
