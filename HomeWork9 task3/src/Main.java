import java.sql.*;

public class Main {

    public static void main(String[] args) {
        try {
            Class.forName("org.h2.Driver").newInstance();
            Connection conn = DriverManager.getConnection("jdbc:h2:tcp://localhost/~/test", "root", "root");
            Statement st = conn.createStatement();

            st.executeUpdate("INSERT INTO EMPLOYEE(id, name) VALUES (1, 'Employee1')");
            st.executeUpdate("INSERT INTO EMPLOYEE(id, name) VALUES (2, 'Employee2')");
            st.executeUpdate("INSERT INTO EMPLOYEE(id, name) VALUES (3, 'Employee3')");
            st.executeUpdate("INSERT INTO EMPLOYEE(id, name) VALUES (4, 'Employee4')");
            st.executeUpdate("INSERT INTO EMPLOYEE(id, name) VALUES (5, 'Employee5')");
            st.executeUpdate("INSERT INTO SALARY (id, value, emp_id) VALUES (1, 4334, 1)");
            st.executeUpdate("INSERT INTO SALARY (id, value, emp_id) VALUES (6, 34, 1)");
            st.executeUpdate("INSERT INTO SALARY (id, value, emp_id) VALUES (7, 43334, 1)");
            st.executeUpdate("INSERT INTO SALARY (id, value, emp_id) VALUES (2, 4352, 2)");
            st.executeUpdate("INSERT INTO SALARY (id, value, emp_id) VALUES (3, 7452, 3)");
            st.executeUpdate("INSERT INTO SALARY (id, value, emp_id) VALUES (4, 7357, 4)");
            st.executeUpdate("INSERT INTO SALARY (id, value, emp_id) VALUES (5, 8647, 5)");

            ResultSet result = st.executeQuery("SELECT e.id as id, e.Name as name, sum(s.VALUE) as sum\n" +
                    "FROM EMPLOYEE e\n" +
                    "JOIN SALARY s on e.ID= s.EMP_ID\n" +
                    "GROUP BY e.id");
            while (result.next()) {
                String name = result.getString("NAME");
                System.out.println(name + " " + result.getDouble("sum"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
