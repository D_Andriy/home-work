package com.geekhub.storage;

public class StorageException extends RuntimeException {

    public StorageException(Throwable cause) {
        super(cause);
    }
}
