package com.geekhub.util;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.util.ResourceBundle;

public class DataSourceFactory {

    public static DataSource getDataSource() {
        MysqlDataSource dataSource = new MysqlDataSource();
        try {
            ResourceBundle properties = ResourceBundle.getBundle("db");
            dataSource.setUrl(properties.getString("db.url"));
            dataSource.setUser(properties.getString("db.user"));
            dataSource.setPassword(properties.getString("db.password"));
        } catch (Exception e) {
            throw new RuntimeException();
        }

        return dataSource;
    }
}
