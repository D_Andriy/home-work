package com.geekhub.servlet.listener;

import com.geekhub.storage.DatabaseStorage;
import com.geekhub.storage.Storage;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppContextListener implements ServletContextListener {

    private static ServletContext servletContext;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            servletContext = servletContextEvent.getServletContext();
            Storage storage = new DatabaseStorage();
            servletContext.setAttribute("DBStorage", storage);
        } catch (Exception e) {

        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        if (servletContextEvent.getServletContext().getAttribute("DBStorage") != null) {
            servletContextEvent.getServletContext().removeAttribute("DBStorage");
        }
    }
}
