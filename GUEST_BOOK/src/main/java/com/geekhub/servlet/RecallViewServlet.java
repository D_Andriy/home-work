package com.geekhub.servlet;

import com.geekhub.objects.Guest;
import com.geekhub.storage.Storage;
import com.geekhub.storage.StorageException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/recall/view")
public class RecallViewServlet extends HttpServlet {

    private final static int GUESTS_PER_PAGE = 7;
    private Storage storage;

    @Override
    public void init(ServletConfig config) {
        storage = (Storage) config.getServletContext().getAttribute("DBStorage");
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        String page = req.getParameter("page");
        resp.setContentType("text/html;charset=UTF-8");
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("<html>");
            sb.append("<body>");
            sb.append("<head>\n" +
                    "<title>Guests recall</title>\n" +
                    "<style>\n" +
                    "table {\n" +
                    "    font-family: arial, sans-serif;\n" +
                    "    border-collapse: collapse;\n" +
                    "    width: 100%;\n" +
                    "}\n" +
                    "\n" +
                    "td, th {\n" +
                    "    border: 1px solid #D3D3D3;\n" +
                    "    text-align: left;\n" +
                    "    padding: 8px;\n" +
                    "}\n" +
                    "\n" +
                    "tr:nth-child(even) {\n" +
                    "    background-color: #D3D3D3;\n" +
                    "}\n" +
                    "</style>\n" +
                    "</head>");
            List<Guest> guests = storage.list(Guest.class);
            appendRecall(guests
                            .stream()
                            .sorted(Comparator.comparing(Guest::getDate, Comparator.reverseOrder()))
                            .skip((extractPageNumber(page) - 1) * GUESTS_PER_PAGE)
                            .limit(GUESTS_PER_PAGE)
                            .collect(Collectors.toList()),
                          sb);
            int pageCount = (int) Math.ceil(guests.size() * 1.0 / GUESTS_PER_PAGE);
            pagination(extractPageNumber(page), pageCount, sb);
            sb.append("</body>");
            sb.append("</html>");
            resp.getWriter().write(sb.toString());
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    public void appendRecall(List<Guest> guests, StringBuilder sb) {
        sb.append("<table>");
        sb.append("<tr>");
        sb.append("<th>Date</th>\n" +
                "    <th>Name</th>\n" +
                "    <th>Rating</th>\n" +
                "    <th>Comment</th>");
        sb.append("</tr>");
        for (Guest guestRecall : guests) {
            sb.append("<tr>");
            sb.append("<td>" + guestRecall.getDate() + "</td>\n " +
                    "<td>" + guestRecall.getName() + "</td> \n" +
                    "<td>" + guestRecall.getRating() + "</td>\n" +
                    "<td>" + guestRecall.getComment() + "</td>");
            sb.append("</tr>");
        }
        sb.append("</table>");
    }

    public void pagination(int pageNumber, int pageCount, StringBuilder sb) {
        sb.append("<table>");
        if (pageNumber != 1) {
            sb.append("<td><a href=\"/recall/view?page=" + 1 + "\">first</a></td>");
        }
        for (int i = 1; i <= pageCount; i++) {
            if (pageNumber == i) {
                sb.append("<td>...</td>");
            } else {
                if (i < pageNumber + 5 && i > pageNumber - 5) {
                    sb.append("<td><a href=\"/recall/view?page=" + i + "\">" + i + "</a></td>");
                }
            }
        }
        if (pageNumber != pageCount) {
            sb.append("<td><a href=\"/recall/view?page=" + pageCount + "\">last</a></td>");
        }
        sb.append("</table>");
    }

    private int extractPageNumber(String page) {
        int pageNumber = (page == null) ? 1 : Integer.parseInt(page);
        return pageNumber;

    }
}
