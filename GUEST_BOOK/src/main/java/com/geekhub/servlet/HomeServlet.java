package com.geekhub.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/*")
public class HomeServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<head>\n" +
                "    <title>Guest book</title>\n" +
                "</head>");
        sb.append("<body>\n" +
                "<h1> Guest book </h1>");
        sb.append("<form action=\"/repository\" method=\"post\" id=\"guestform\">\n" +
                "    Name: <input type=\"text\" name=\"name\">\n" +
                "    <input type=\"submit\"  value=\"send\">\n" +
                "</form>");
        sb.append("<br>");
        sb.append("<textarea rows=\"4\" cols=\"50\" name=\"comment\" form=\"guestform\">\n" +
                "Enter recall here...</textarea>");
        sb.append("<br>");
        sb.append("<select name=\"rating\" form=\"guestform\">\n" +
                "  <option value=\"1\">1</option>\n" +
                "  <option value=\"2\">2</option>\n" +
                "  <option value=\"3\">3</option>\n" +
                "  <option value=\"4\">4</option>\n" +
                "  <option value=\"5\">5</option>\n" +
                "</select>");
        sb.append("</body>");
        sb.append("</html>");
        resp.getWriter().write(sb.toString());
    }
}
