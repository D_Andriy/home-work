package com.geekhub.servlet;

import com.geekhub.objects.Guest;
import com.geekhub.storage.Storage;
import com.geekhub.storage.StorageException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.time.LocalDate;

@WebServlet("/repository")
public class RepositoryServlet extends HttpServlet {

    private Storage storage;

    @Override
    public void init(ServletConfig config) {
        storage = (Storage) config.getServletContext().getAttribute("DBStorage");
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        String name = req.getParameter("name");
        String comment = req.getParameter("comment");
        String rating = req.getParameter("rating");
        try {
            LocalDate localDate = LocalDate.now();
            Guest guest = new Guest();
            guest.setName(name);
            guest.setComment(comment);
            guest.setRating(Integer.parseInt(rating));
            guest.setDate(Date.valueOf(localDate));
            storage.save(guest);

            resp.sendRedirect("/recall/view");
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }
}
