package com.geekhub;

import java.util.ArrayList;
import java.util.List;

public class Car {

    private Engine engine;

    private List<Wheel> wheels = new ArrayList<>();

    public void setWheels(List<Wheel> wheels) {
        this.wheels = wheels;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

}
