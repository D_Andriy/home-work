package com.geekhub.sourceprovider;

import java.util.List;

import static java.util.stream.Collectors.joining;

public abstract class SourceProvider {

    public abstract boolean isResolve(String pathToSource);

    protected abstract List<String> parseContent(String path) throws ParseContentException;

    public String processLines(String path, String wordsForFinding) throws ParseContentException {
        String result = parseContent(path).stream()
                .filter(line -> line.toLowerCase().contains(wordsForFinding))
                .map(line -> addAsteriskAround(line, wordsForFinding))
                .collect(joining("\n"));

        return result;
    }


    private String addAsteriskAround(String line, String wordsForFinding) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(line);
        int wordsIndex = stringBuilder.indexOf(wordsForFinding);
        stringBuilder.insert(wordsIndex, "*");
        stringBuilder.insert(wordsIndex + wordsForFinding.length() + 1, "*");

        return stringBuilder.toString();
    }
}
