package com.geekhub.sourceprovider;

public class ParseContentException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Some problems with content!";
    }
}
