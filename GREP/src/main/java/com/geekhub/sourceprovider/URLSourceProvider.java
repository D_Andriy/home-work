package com.geekhub.sourceprovider;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.apache.commons.io.IOUtils.readLines;

public class URLSourceProvider extends SourceProvider {

    private static final String[] SCHEMES = {"http", "https", "ftp"};

    @Override
    public boolean isResolve(String pathToSource) {
        final URL url;
        try {
            url = new URL(pathToSource);
        } catch (Exception e1) {
            return false;
        }
        return Arrays.stream(SCHEMES)
                .anyMatch(protocol -> protocol.equals(url.getProtocol()));
    }

    @Override
    public List<String> parseContent(String url) {
        try (InputStream input = new URL(url).openStream()) {

            return readLines(input);
        } catch (IOException e) {
            throw new ParseContentException();
        }
    }
}
