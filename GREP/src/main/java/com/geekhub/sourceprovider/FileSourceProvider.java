package com.geekhub.sourceprovider;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.apache.commons.io.FileUtils.readLines;

public class FileSourceProvider extends SourceProvider {

    @Override
    public boolean isResolve(String pathToSource) {
        return new File(pathToSource).isFile();
    }

    @Override
    public List<String> parseContent(String path) throws ParseContentException {
        try {
            return readLines(new File(path), "UTF-8");
        } catch (IOException e) {
            throw new ParseContentException();
        }
    }
}
