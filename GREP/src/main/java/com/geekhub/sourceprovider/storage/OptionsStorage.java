package com.geekhub.sourceprovider.storage;

import org.apache.commons.cli.*;

public class OptionsStorage {

    public Options getOptions() {
        Option wordsOption = new Option("w", "word", true, "search by words");
        wordsOption.setArgs(3);

        Options options = new Options();
        options
                .addOption(wordsOption)
                .addOption("u", "url", true, "path to URL")
                .addOption("f", "file", true, "path to file")
                .addOption("r", "regex", true, "search by regEX")
                .addOption("h", "help", false, "user help");

        return options;
    }
}
