package com.geekhub;

import com.geekhub.sourceprovider.storage.OptionsStorage;
import org.apache.commons.cli.*;

public class Main {

    private static final char HELP_OPTION = 'h';
    private static final char WORD_OPTION = 'w';
    private static final char REGEX_OPTION = 'r';
    private static final char FILE_OPTION = 'f';
    private static final char URL_OPTION = 'u';

    public static void main(String[] args) throws Exception {
        OptionsStorage optionsStorage = new OptionsStorage();
        CommandLineParser parser = new PosixParser();
        CommandLine cmd;
        cmd = parser.parse(optionsStorage.getOptions(), args);
        LineBuilder lineBuilder = new LineBuilder();

        if (cmd.hasOption(HELP_OPTION)) {
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("java -jar grep.jar", optionsStorage.getOptions());
            return;
        } else if (cmd.hasOption(WORD_OPTION) && cmd.hasOption(URL_OPTION)) {
            lineBuilder
                    .setValid()
                    .addWordOption(cmd.getOptionValues(WORD_OPTION))
                    .addURLPathOption(cmd.getOptionValues(URL_OPTION));
        } else if (cmd.hasOption(WORD_OPTION) && cmd.hasOption(FILE_OPTION)) {
            lineBuilder
                    .setValid()
                    .addWordOption(cmd.getOptionValues(WORD_OPTION))
                    .addFilePathOption(cmd.getOptionValues(FILE_OPTION));
        } else if (cmd.hasOption(REGEX_OPTION) && cmd.hasOption(URL_OPTION)) {
            lineBuilder
                    .setValid()
                    .addRegexOption(cmd.getOptionValues(REGEX_OPTION))
                    .addURLPathOption(cmd.getOptionValues(URL_OPTION));
        } else if (cmd.hasOption(REGEX_OPTION) && cmd.hasOption(FILE_OPTION)) {
            lineBuilder
                    .setValid()
                    .addRegexOption(cmd.getOptionValues(REGEX_OPTION))
                    .addFilePathOption(cmd.getOptionValues(FILE_OPTION));
        }
        if (lineBuilder.isValid()) {
            System.out.println(lineBuilder.getProcessedLines());
        }else {
            throw new InvalidInputCommandException();
        }
    }
}

