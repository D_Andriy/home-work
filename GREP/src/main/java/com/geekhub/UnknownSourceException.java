package com.geekhub;

public class UnknownSourceException extends Exception {
    @Override
    public String getMessage() {
        return "Unknown source";
    }
}
