package com.geekhub;

import com.geekhub.sourceprovider.FileSourceProvider;
import com.geekhub.sourceprovider.URLSourceProvider;

import java.util.stream.Stream;

import static java.util.Arrays.stream;

public class LineBuilder {
    private String wordsForFinding;
    private String pathToSource;
    private boolean isValid;

    public LineBuilder() {
        this.isValid = false;
    }

    public LineBuilder addWordOption(String[] wordsForFinding) {
        StringBuilder stringBuilder = new StringBuilder();
        stream(wordsForFinding)
                .forEach(word -> stringBuilder.append(word).append(""));
        this.wordsForFinding = stringBuilder.toString().toLowerCase();
        return this;
    }

    public LineBuilder addRegexOption(String[] wordsForFinding) {
        this.wordsForFinding = wordsForFinding[0].replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
        return this;
    }

    public LineBuilder addURLPathOption(String[] pathToSource) throws UnknownSourceException {
        if (new URLSourceProvider().isResolve(pathToSource[0])) {
            this.pathToSource = pathToSource[0];
        } else {
            throw new UnknownSourceException();
        }
        return this;
    }

    public LineBuilder addFilePathOption(String[] pathToSource) throws UnknownSourceException {
        if (new FileSourceProvider().isResolve(pathToSource[0])) {
            this.pathToSource = pathToSource[0];
        } else {
            throw new UnknownSourceException();
        }
        return this;
    }


    public String getProcessedLines() throws UnknownSourceException {
        return Stream.of(new FileSourceProvider(), new URLSourceProvider())
                .filter(sp -> sp.isResolve(pathToSource))
                .findFirst()
                .map(sp -> sp.processLines(pathToSource, wordsForFinding))
                .orElseThrow(UnknownSourceException::new);
    }

    public boolean isValid() {
        return isValid;
    }

    public LineBuilder setValid() {
        this.isValid = true;
        return this;
    }
}
