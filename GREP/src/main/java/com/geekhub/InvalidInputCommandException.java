package com.geekhub;

public class InvalidInputCommandException extends Exception {
    @Override
    public String getMessage() {
        return "Use option -h for help";
    }
}
