package com.geekhub.sourceprovider;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class SourceProviderTest {

    @Test
    public void shouldReturnTrueElsePathIsFileOrFalse() {
        SourceProvider fileSourceProvider = Mockito.mock(FileSourceProvider.class);
        when(fileSourceProvider.isResolve("pathToString")).thenReturn(true);
        assertTrue(fileSourceProvider.isResolve("pathToString"));
        when(fileSourceProvider.isResolve("pathToString")).thenReturn(false);
        assertFalse(fileSourceProvider.isResolve("pathToString"));
    }

    @Test
    public void shouldReturnTrueElsePathIsUrlOrFalse() {
        SourceProvider urlSourceProvider = Mockito.mock(URLSourceProvider.class);
        when(urlSourceProvider.isResolve("pathToString")).thenReturn(true);
        assertTrue(urlSourceProvider.isResolve("pathToString"));
        when(urlSourceProvider.isResolve("pathToString")).thenReturn(false);
        assertFalse(urlSourceProvider.isResolve("pathToString"));
    }

    @Test
    public void shouldGetAllLineFromFile() {
        List<String> content = Arrays.asList("i use mockito", "testing: parsContent method");
        SourceProvider fileSourceProvider = Mockito.mock(FileSourceProvider.class);
        when(fileSourceProvider.parseContent("pathToString")).thenReturn(content);
        assertEquals(content, fileSourceProvider.parseContent("pathToString"));
        assertNotEquals(content, fileSourceProvider.parseContent("incorrectPath"));
    }

    @Test
    public void shouldGetAllLineFromURL() {
        List<String> content = Arrays.asList("i use mockito", "testing: parsContent method");
        SourceProvider urlSourceProvider = Mockito.mock(URLSourceProvider.class);
        when(urlSourceProvider.parseContent("pathToString")).thenReturn(content);
        assertEquals(content, urlSourceProvider.parseContent("pathToString"));
        assertNotEquals(content, urlSourceProvider.parseContent("incorrectPath"));
    }
}