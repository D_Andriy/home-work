package com.geekhub;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LineBuilderTest {

    @Test
    public void shouldGetLineByWord() throws UnknownSourceException {
        LineBuilder lineBuilder = new LineBuilder();
        String actualResult = lineBuilder
                .setValid()
                .addFilePathOption(new String[]{"src/test/resources/com/geekhub/1.txt"})
                .addWordOption(new String[]{"use"})
                .getProcessedLines();
        assertEquals("I *use* Spring3.0", actualResult);
    }

    @Test
    public void shouldGetLineByRegex() throws UnknownSourceException {
        LineBuilder lineBuilder = new LineBuilder();
        String actualResult = lineBuilder
                .setValid()
                .addFilePathOption(new String[]{"src/test/resources/com/geekhub/1.txt"})
                .addRegexOption(new String[]{"([use])"})
                .getProcessedLines();
        assertEquals("I *use* Spring3.0", actualResult);
    }

}