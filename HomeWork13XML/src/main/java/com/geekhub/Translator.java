package com.geekhub;

import com.geekhub.source.URLSourceProvider;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.URLEncoder;

public class Translator {

    private String apiKey;

    private static final String TRANSLATION_DIRECTION = "ru";

    private URLSourceProvider urlSourceProvider;

    public void setUrlSourceProvider(URLSourceProvider urlSourceProvider) {
        this.urlSourceProvider = urlSourceProvider;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String translate(String original) throws Exception {
        return parseContent(urlSourceProvider.load(prepareURL(original)));
    }

    private String prepareURL(String text) {
        return "https://translate.yandex.net/api/v1.5/tr/translate?key="
                + apiKey + "&text=" + encodeText(text) + "&lang=" + TRANSLATION_DIRECTION;
    }

    private String parseContent(String content) throws Exception {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.parse(new InputSource(new StringReader(content)));
        doc.getDocumentElement().normalize();
        return doc.getElementsByTagName("text").item(0).getTextContent();
    }

    private String encodeText(String text) {
        try {
            return URLEncoder.encode(text, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
