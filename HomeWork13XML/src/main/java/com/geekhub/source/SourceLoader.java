package com.geekhub.source;

import java.util.ArrayList;
import java.util.List;

public class SourceLoader {

    private FileSourceProvider fileSourceProvider;

    private URLSourceProvider urlSourceProvider;

    private List<SourceProvider> sourceProviders = new ArrayList<>();

    public void setFileSourceProvider(FileSourceProvider fileSourceProvider) {
        this.fileSourceProvider = fileSourceProvider;
        sourceProviders.add(fileSourceProvider);
    }

    public void setUrlSourceProvider(URLSourceProvider urlSourceProvider) {
        this.urlSourceProvider = urlSourceProvider;
        sourceProviders.add(urlSourceProvider);
    }

    public String loadSource(String pathToSource) throws Exception {
        for (SourceProvider sp : sourceProviders) {
            if (sp.isAllowed(pathToSource)) {
                return sp.load(pathToSource);
            } else {
                throw new Exception();
            }
        }

        return null;
    }
}
