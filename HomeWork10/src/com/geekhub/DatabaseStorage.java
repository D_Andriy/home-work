package com.geekhub;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseStorage {

    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    public void quantityRegion() {
        try {
            Statement statement = connection.createStatement();
            String sql = "SELECT c.name AS country, COUNT(r.country_id) AS region_count\n" +
                    "FROM country c\n" +
                    "JOIN region r ON c.id = r.country_id\n" +
                    "GROUP BY country\n" +
                    "ORDER BY region_count DESC, country ASC \n" +
                    "LIMIT 5";
            ResultSet result = statement.executeQuery(sql);
            while (result.next()) {
                String countryName = result.getString("country");
                String regionCount = result.getString("region_count");

                System.out.println(countryName + " " + regionCount);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void quantityCity() {
        try {
            Statement statement = connection.createStatement();
            String sql = "SELECT c.name AS country, COUNT(ct.region_id) AS city_count\n" +
                    " FROM country c\n" +
                    "JOIN region r ON r.country_id = c.id\n" +
                    "JOIN city ct ON ct.region_id = r.id\n" +
                    "GROUP BY c.id\n" +
                    " ORDER BY city_count DESC, country ASC\n" +
                    " LIMIT 5\n";
            ResultSet result = statement.executeQuery(sql);
            while (result.next()) {
                String countryName = result.getString("country");
                String cityCount = result.getString("city_count");

                System.out.println(countryName + " " + cityCount);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void allCountry() {
        try {
            Statement statement = connection.createStatement();
            String sql = "SELECT c.name AS country,\n" +
                    "COUNT(DISTINCT r.id) AS region_count,\n" +
                    "COUNT(ct.id)   AS city_count\n" +
                    "FROM country c \n" +
                    "JOIN region r ON r.country_id = c.id\n" +
                    "JOIN city ct ON ct.region_id = r.id\n" +
                    "GROUP BY c.id\n" +
                    "ORDER BY region_count DESC, city_count DESC , country ASC";
            ResultSet result = statement.executeQuery(sql);
            while (result.next()) {
                String countryName = result.getString("country");
                String regionCount = result.getString("region_count");
                String cityCount = result.getString("city_count");

                System.out.println(countryName + " " + regionCount + " " + cityCount);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
