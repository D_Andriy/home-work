package com.geekhub;

import java.sql.Connection;
import java.sql.DriverManager;

public class Test {

    public static void main(String[] args) throws Exception {
        Connection connection = createConnection("root", "root", "geo");
        DatabaseStorage storage = new DatabaseStorage(connection);

        System.out.println("Top 5 country by region: ");
        storage.quantityRegion();
        System.out.println("\n Top 5 country by city: ");
        storage.quantityCity();
        System.out.println("\n All country, region and city: ");
        storage.allCountry();

        connection.close();
    }

    private static Connection createConnection(String login, String password, String dbName) throws Exception {


        return DriverManager.getConnection("jdbc:mysql://localhost/" + dbName, login, password);
    }
}
