package com.geekhub.source;

import java.util.ArrayList;
import java.util.List;

public class SourceLoader {

    private List<SourceProvider> sourceProviders = new ArrayList<>();

    public SourceLoader(FileSourceProvider fileSourceProvider, URLSourceProvider urlSourceProvider) {
        this.sourceProviders.add(fileSourceProvider);
        this.sourceProviders.add(urlSourceProvider);
    }

    public String loadSource(String pathToSource) throws Exception {
        for (SourceProvider sp : sourceProviders) {
            if (sp.isAllowed(pathToSource)) {
                return sp.load(pathToSource);
            } else {
                throw new Exception();
            }
        }

        return null;
    }
}
