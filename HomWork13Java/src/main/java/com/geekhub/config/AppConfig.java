package com.geekhub.config;

import com.geekhub.Translator;
import com.geekhub.source.FileSourceProvider;
import com.geekhub.source.SourceLoader;
import com.geekhub.source.URLSourceProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:translator.properties")
public class AppConfig {

    @Bean
    public FileSourceProvider fileSourceProvider() {
        return new FileSourceProvider();
    }

    @Bean
    public URLSourceProvider urlSourceProvider() {
        return new URLSourceProvider();
    }

    @Bean
    public SourceLoader sourceLoader(FileSourceProvider fileSourceProvider, URLSourceProvider urlSourceProvider) {
        return new SourceLoader(fileSourceProvider, urlSourceProvider);
    }

    @Bean
    public Translator translator(URLSourceProvider urlSourceProvider) {
        return new Translator(urlSourceProvider);
    }
}
