package com.geekhub;

import com.geekhub.config.AppConfig;
import com.geekhub.source.SourceLoader;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        SourceLoader sourceLoader = context.getBean(SourceLoader.class);
        Translator translator = context.getBean(Translator.class);
        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while (!"exit".equals(command)) {
            try {
                String source = sourceLoader.loadSource(command);
                String translation = translator.translate(source);
                System.out.println("Original: " + source);
                System.out.println("Translation: " + translation);
            } catch (Exception e) {
                System.out.println("enter another path to translation:");
            }
            command = scanner.next();
        }
    }
}
