package com.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@PropertySource("classpath:car.properties")
public class Car {

    @Autowired
    private Engine engine;

    private List<Wheel> wheels = new ArrayList<>();

    @Autowired
    public void setWheels(Wheel wheel) {
        for (int i = 0; i < 4; i++) {
            this.wheels.add(wheel);
        }
    }
}
