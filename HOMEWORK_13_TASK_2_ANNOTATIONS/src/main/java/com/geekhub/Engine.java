package com.geekhub;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Engine {

    @Value("${car.engine}")
    private double engineCapacity;
}
