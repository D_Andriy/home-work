package com.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Wheel {

    @Autowired
    @Qualifier("SummerTyres")
    private Tyres tyre;
}
