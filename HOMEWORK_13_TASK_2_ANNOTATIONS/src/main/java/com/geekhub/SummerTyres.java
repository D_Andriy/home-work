package com.geekhub;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component("SummerTyres")
public class SummerTyres extends Tyres {

    @Value("${car.summerTyresSize}")
    private int size;

    @Value("${car.summerTyresName}")
    private String name;
}
