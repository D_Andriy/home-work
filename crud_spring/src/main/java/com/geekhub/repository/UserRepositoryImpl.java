package com.geekhub.repository;

import com.geekhub.domain.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final AtomicInteger idGenerator = new AtomicInteger();
    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User find(int id) {
        for (User user : users) {
            if (id == user.getId()) {
                return user;
            }
        }
        return null;
    }

    @Override
    public void save(User user) {
        Integer id = user.getId();
        if (id == null) {
            id = idGenerator.incrementAndGet();
            user.setId(id);
            users.add(user);
        } else {
            User userToUpdate = find(id);
            userToUpdate.setAge(user.getAge());
            userToUpdate.setFirstName(user.getFirstName());
            userToUpdate.setLastName(user.getLastName());
            users.set(id - 1, user);
        }
    }

    @Override
    public void delete(int id) {
        User userToDelete = null;
        for (User user : users) {
            if (id == user.getId()) {
                userToDelete = user;
                break;
            }
        }
        users.remove(userToDelete);
    }
}
