package com.geekhub.repository;

import com.geekhub.domain.User;

import java.util.List;

public interface UserRepository {

    List<User> findAll();

    User find(int id);

    void save(User user);

    void delete(int id);
}
