package com.geekhub.service;

import com.geekhub.domain.User;

import java.util.List;

public interface UserService {

    List<User> findAllUsers();

    User findUser(int id);

    void saveUser(User user);

    void deleteUser(int id);
}
