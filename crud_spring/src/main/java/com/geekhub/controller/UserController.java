package com.geekhub.controller;

import com.geekhub.domain.User;
import com.geekhub.pagination.PageRequest;
import com.geekhub.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.geekhub.pagination.PaginationUtils.getPage;

@Controller
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String list(Model model, @RequestParam(defaultValue = "1", required = false) Integer page, @RequestParam(defaultValue = "7", required = false) Integer perPage) {
        List<User> users = userService.findAllUsers();
        model.addAttribute("page", getPage(users, new PageRequest(page, perPage)));
        return "list";
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String saveUser(@RequestParam("firstName") String firstName,
                           @RequestParam("lastName") String lastName,
                           @RequestParam("age") String age) {
        User user = new User(firstName, lastName, Integer.parseInt(age));
        userService.saveUser(user);
        return "redirect:/users/" + user.getId();
    }

    @RequestMapping(value = "/users/{userId}/edit", method = RequestMethod.GET)
    public String editUser(@PathVariable Integer userId, Model model) {
        model.addAttribute("user", userService.findUser(userId));
        return "update";
    }

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.POST)
    public String updateUser(@PathVariable Integer userId,
                             @RequestParam("firstName") String firstName,
                             @RequestParam("lastName") String lastName,
                             @RequestParam("age") String age) {
        User user = new User(userId, firstName, lastName, Integer.parseInt(age));
        userService.saveUser(user);
        return "redirect:/users";
    }

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
    public String showUser(Model model, @PathVariable Integer userId) {
        model.addAttribute("user", userService.findUser(userId));
        return "show";
    }

    @RequestMapping(value = "/users/{userId}/delete", method = RequestMethod.GET)
    public String deleteUser(@PathVariable Integer userId) {
        userService.deleteUser(userId);
        return "redirect:/users";
    }

    @RequestMapping(value = "/users/create", method = RequestMethod.GET)
    public String createUser() {
        return "create";
    }
}
