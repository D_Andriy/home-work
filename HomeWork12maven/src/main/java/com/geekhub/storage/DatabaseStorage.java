package com.geekhub.storage;

import com.geekhub.objects.Entity;
import com.geekhub.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

public class DatabaseStorage implements Storage {

    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws StorageException {
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws StorageException, IllegalAccessException, InstantiationException {
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase();
        try (Statement statement = connection.createStatement()) {
            return extractResult(clazz, statement.executeQuery(sql));
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws StorageException {
        String sql = "DELETE FROM " + entity.getClass().getSimpleName().toLowerCase() + " WHERE id = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            int i = statement.executeUpdate(sql);
            if (i == 1) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> int delete(Class<T> clazz) throws StorageException {
        String sql = "DELETE FROM " + clazz.getSimpleName().toLowerCase();
        try (Statement statement = connection.createStatement()) {
            int count = statement.executeUpdate(sql);
            return count;
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws StorageException, IllegalAccessException, SQLException {
        if (entity.isNew()) {
            StringBuilder sql = new StringBuilder("INSERT INTO ").append(entity.toString().toLowerCase()).append(" (");
            StringBuilder placeholders = new StringBuilder();

            for (Iterator<String> iterator = prepareEntity(entity).keySet().iterator(); iterator.hasNext(); ) {
                sql.append(iterator.next());
                placeholders.append("?");
                if (iterator.hasNext()) {
                    sql.append(",");
                    placeholders.append(",");
                }
            }
            sql.append(") VALUES (").append(placeholders).append(")");
            PreparedStatement preparedStatement = connection.prepareStatement(sql.toString().toLowerCase(), Statement.RETURN_GENERATED_KEYS);

            int count = 1;

            for (Object value : prepareEntity(entity).values()) {
                preparedStatement.setObject(count++, value);
            }
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            generatedKeys.next();
            entity.setId(generatedKeys.getInt(1));
        } else {
            StringBuilder sql = new StringBuilder("UPDATE ").append(entity.toString().toLowerCase()).append(" SET ");
            StringBuilder placeholders = new StringBuilder();

            for (Iterator<String> iterator = prepareEntity(entity).keySet().iterator(); iterator.hasNext(); ) {
                placeholders.append(iterator.next()).append("=").append("?");
                if (iterator.hasNext()) {
                    placeholders.append(",");
                }
            }
            sql.append(placeholders).append(" WHERE id=").append(entity.getId());
            PreparedStatement preparedStatement = connection.prepareStatement(sql.toString().toLowerCase());

            int count = 1;

            for (Object value : prepareEntity(entity).values()) {
                preparedStatement.setObject(count++, value);
            }
            preparedStatement.executeUpdate();
        }
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<>();
        for (Field field : entity.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(Ignore.class)) {
                map.put(field.getName(), field.get(entity));
            }
        }

        return map;
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> list = new ArrayList<>();
        while (resultSet.next()) {
            T t = clazz.newInstance();
            for (Field f : clazz.getDeclaredFields()) {
                f.setAccessible(true);
                if (!f.isAnnotationPresent(Ignore.class)) {
                    f.set(t, resultSet.getObject(f.getName()));
                }
            }
            t.setId(resultSet.getInt("id"));
            list.add(t);
        }

        return list;
    }
}
