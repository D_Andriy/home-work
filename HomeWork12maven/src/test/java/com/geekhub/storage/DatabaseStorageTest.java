package com.geekhub.storage;

import com.geekhub.objects.Cat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

import static com.geekhub.storage.DbTestExecutor.execute;
import static com.geekhub.storage.EntityDataProvider.getCat;
import static com.geekhub.storage.EntityDataProvider.getCat2;
import static org.junit.Assert.*;

public class DatabaseStorageTest {

    private static Connection connection;

    private Storage storage = new DatabaseStorage(connection);

    @BeforeClass
    public static void createConnection() throws Exception {
        Class.forName("org.h2.Driver").newInstance();
        connection = DriverManager.getConnection("jdbc:h2:tcp://localhost/~/test", "root", "root");
        try {
            execute(connection, "CREATE TABLE CAT \n" +
                    "(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,\n" +
                    "name VARCHAR(255),\n" +
                    "age INT)");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void closeConnection() throws Exception {
        execute(connection, "DROP TABLE CAT");
        connection.close();
    }

    @After
    public void cleanTables() throws Exception {
        execute(connection, "DELETE FROM CAT");
        execute(connection, "ALTER TABLE CAT ALTER COLUMN id RESTART WITH 1");
    }

    @Test
    public void shouldCheckDataThatInsertedReturnByGet() throws Exception {
        storage.save(getCat());
        assertNotNull(storage.get(Cat.class, 1));
        assertEquals(getCat(), storage.get(Cat.class, 1));
    }

    @Test
    public void shouldReturnCorrectDataByList() throws Exception {
        List<Cat> cats = new ArrayList<>();
        cats.add(getCat());
        cats.add(getCat2());
        storage.save(getCat());
        storage.save(getCat2());
        assertEquals(cats, storage.list(Cat.class));
        assertEquals(cats.size(), storage.list(Cat.class).size());
    }

    @Test
    public void shouldSaveAndUpdateData() throws Exception {
        Cat newCat = new Cat();
        newCat.setName("cat");
        newCat.setAge(7);
        newCat.setId(1);

        storage.save(getCat());
        storage.save(getCat2());
        assertEquals(getCat(), storage.get(Cat.class, 1));
        assertEquals(getCat2(), storage.get(Cat.class, 2));

        storage.save(newCat);
        assertEquals(newCat, storage.get(Cat.class, 1));
        assertEquals(getCat2(), storage.get(Cat.class, 2));
    }

    @Test
    public void shouldDeleteAllDataFromTable() throws Exception {
        storage.save(getCat());
        storage.save(getCat2());
        assertEquals(2, storage.delete(Cat.class));
        assertEquals(0, storage.delete(Cat.class));
        assertTrue(storage.list(Cat.class).isEmpty());
    }

    @Test
    public void shouldDeleteDataFromTable() throws Exception {
        Cat deletedCat = new Cat();
        deletedCat.setName("cat");
        deletedCat.setAge(7);
        deletedCat.setId(1);

        storage.save(getCat());
        assertTrue(storage.delete(deletedCat));
        assertFalse(storage.delete(deletedCat));
    }
}