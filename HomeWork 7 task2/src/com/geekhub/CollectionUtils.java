package com.geekhub;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Optional.ofNullable;


public class CollectionUtils {


    private CollectionUtils() {
    }

    public static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> result = new ArrayList<>();
        for (E element : elements) {
            if (filter.test(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element)) {
                return true;
            }
        }
        return false;
    }

    public static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (!predicate.test(element)) {
                return false;
            }
        }
        return true;
    }

    public static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        return !anyMatch(elements, predicate);
    }

    public static <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction) {
        List<R> result = new ArrayList<>();
        for (T element : elements) {
            result.add(mappingFunction.apply(element));
        }
        return result;
    }

    public static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        return reduce(elements, (a, b) -> {
            if (comparator.compare(a, b) == 1){
                return a;
            } else {
                return b;
            }
        });
    }

    public static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        return max(elements, (b, a) -> comparator.compare(b,a));
//                reduce(elements, (a, b) -> {
//            if (comparator.compare(a, b) == -1) return a;
//            else return b;
//        });
    }

    public static <E> List<E> distinct(List<E> elements) {
        List<E> result = new ArrayList<>();
        for (E element : elements) {
            if (!result.contains(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E element : elements) {
            consumer.accept(element);
        }
    }

    public static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        Optional<E> result = Optional.of(elements.get(0));
        for (E element : elements) {
                result = ofNullable(accumulator.apply(result.get(), element));
            }

        return result;
    }

    public static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E result = seed;
         for (E element : elements){
                     result = accumulator.apply(result, element);
        }
        return result;
    }

    public static <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate) {
        return groupBy(elements, predicate::test);
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier) {
        return toMap(elements, classifier, Arrays::asList, (a, b) -> {
            List<T> list = new ArrayList<>();
            list.addAll(a);
            list.addAll(b);
            return list;
        });
    }

    public static <T, K, U> Map<K, U> toMap(List<T> elements,
                                            Function<T, K> keyFunction,
                                            Function<T, U> valueFunction,
                                            BinaryOperator<U> mergeFunction) {
        Map<K, U> result = new HashMap<>();
        for (T element : elements) {
            K key = keyFunction.apply(element);
            U value = valueFunction.apply(element);
            if (result.containsKey(key)){
                result.put(key, mergeFunction.apply(result.get(key), value));
            } else {
                result.put(key, value);
            }
        }
        return result;
    }
}
