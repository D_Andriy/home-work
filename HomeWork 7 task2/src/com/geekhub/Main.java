package com.geekhub;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.BinaryOperator;

import static java.util.Arrays.asList;

public class Main {
    public static void main(String[] args) {
        List<Element> elements = getElements();

        System.out.println(CollectionUtils.filter(elements, a -> a.getId() == 1));
        System.out.println(CollectionUtils.anyMatch(elements, a -> a.getId() == 2));
        System.out.println(CollectionUtils.anyMatch(elements, a -> a.getId() == 10));
        System.out.println(CollectionUtils.allMatch(asList(new Element(1, "A"), new Element(1, "E")), a -> a.getId() == 1));
        System.out.println(CollectionUtils.allMatch(elements, a -> a.getId() == 1));
        System.out.println(CollectionUtils.noneMatch(elements, a -> a.getId() == 10));
        System.out.println(CollectionUtils.noneMatch(elements, a -> a.getId() == 1));
        System.out.println(CollectionUtils.map(elements, Element::getName));
        Comparator<Element> comparator = (a, b) -> a.getId() == b.getId() ? 0 : a.getId() > b.getId() ? 1 : -1;
        System.out.println(CollectionUtils.min(elements, comparator));
        System.out.println(CollectionUtils.max(elements, comparator));
        System.out.println(CollectionUtils.distinct(elements));
        CollectionUtils.forEach(elements, a -> a.setName("pp"));
        System.out.println(elements);
        elements = getElements();
        BinaryOperator<Element> accumulator = (Element a, Element b) -> new Element(10, a.getName() + b.getName());
        System.out.println(CollectionUtils.reduce(elements, accumulator));
        System.out.println("reduce " + CollectionUtils.reduce(new Element(-1, "-1"), elements, accumulator));
        System.out.println(CollectionUtils.partitionBy(elements, b -> b.getId() == 1));
        System.out.println(CollectionUtils.groupBy(elements, Element::getId));
        System.out.println(CollectionUtils.toMap(elements, Element::getId, Element::getName, (a, b) -> a + b));
    }

    private static List<Element> getElements() {
        List<Element> elements = new ArrayList<>();
        elements.add(new Element(1, "A"));
        elements.add(new Element(2, "B"));
        elements.add(new Element(3, "C"));
        elements.add(new Element(4, "D"));
        elements.add(new Element(1, "E"));
        elements.add(new Element(5, "F"));
        elements.add(new Element(2, "G"));
        elements.add(new Element(6, "H"));
        elements.add(new Element(7, "I"));
        elements.add(new Element(6, "J"));
        elements.add(new Element(8, "K"));
        elements.add(new Element(9, "L"));
        elements.add(new Element(0, "M"));
        elements.add(new Element(0, "M"));
        return elements;
    }
}
