package com.geekhub.storage;

import java.sql.Connection;
import java.sql.Statement;

public class DbTestExecutor {

    public static void execute(Connection connection, String sql) throws Exception {
        Statement statement = connection.createStatement();
        statement.execute(sql);
        statement.close();
    }

}
