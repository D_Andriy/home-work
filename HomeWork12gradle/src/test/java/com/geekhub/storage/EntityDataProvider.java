package com.geekhub.storage;

import com.geekhub.objects.Cat;

import java.util.Random;

public class EntityDataProvider {

    public static Cat getCat() {
        Cat cat = new Cat();
        cat.setName("cat");
        cat.setAge(5);
        return cat;
    }

    public static Cat getCat2() {
        Cat cat = new Cat();
        cat.setName("cat2");
        cat.setAge(5);
        return cat;
    }
}
