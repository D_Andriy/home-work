package com.geekhub.translator;

import com.geekhub.translator.core.language.Language;
import com.geekhub.translator.core.language.LanguageDetector;
import com.geekhub.translator.core.language.LanguageDetectorException;
import com.geekhub.translator.util.IOUtils;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;

import static com.geekhub.translator.util.EncodingUtils.ENCODING;

public class YandexLanguageDetector implements LanguageDetector {
    private static final String YANDEX_LANGUAGE_DETECTOR_API_URL = "https://translate.yandex.net/api/v1.5/tr.json/detect?key=%s&text=%s";
    private static final String DETECTION_LABEL = "lang";
    private final String apiKey;

    public YandexLanguageDetector(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public Language detect(String text) throws Exception {
        final String response = retrieveResponse(new URL(String.format(YANDEX_LANGUAGE_DETECTOR_API_URL, apiKey, text)));

        JSONObject jsonObj = new JSONObject(response);
        return Language.find(jsonObj.get(DETECTION_LABEL).toString());
    }

    private static String retrieveResponse(URL url) throws LanguageDetectorException, IOException {
        final HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
        uc.setRequestProperty("Content-Type", "text/plain; charset=" + ENCODING);
        uc.setRequestProperty("Accept-Charset", ENCODING);
        uc.setRequestMethod("GET");

        try {
            final int responseCode = uc.getResponseCode();
            final String result = IOUtils.toString(uc.getInputStream());
            if (responseCode != 200) {
                throw new LanguageDetectorException("Error from Yandex API: " + result);
            }
            return result;
        } finally {
            uc.disconnect();
        }
    }
}
