package com.geekhub.translator.core.language;

public class LanguageDetectorException extends Exception {
    private String message;

    public LanguageDetectorException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
