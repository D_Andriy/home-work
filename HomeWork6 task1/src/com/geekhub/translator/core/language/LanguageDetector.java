package com.geekhub.translator.core.language;

public interface LanguageDetector {

    Language detect(String text) throws Exception;
}
