package com.geekhub.translator.core;

public class TranslatorException extends Exception {
    private String message;

    public TranslatorException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
