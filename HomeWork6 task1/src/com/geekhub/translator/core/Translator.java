package com.geekhub.translator.core;

import java.io.IOException;
import java.net.MalformedURLException;

public interface Translator {

    Translation translate(TranslationRequest translationRequest) throws Exception;
}
