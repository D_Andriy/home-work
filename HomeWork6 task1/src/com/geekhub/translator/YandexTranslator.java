package com.geekhub.translator;

import com.geekhub.translator.core.Translation;
import com.geekhub.translator.core.TranslationRequest;
import com.geekhub.translator.core.Translator;
import com.geekhub.translator.core.TranslatorException;
import com.geekhub.translator.core.language.Language;
import com.geekhub.translator.core.language.LanguageDetector;
import com.geekhub.translator.util.IOUtils;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.net.URL;

public class YandexTranslator implements Translator {
    private static final String YANDEX_TRANSLATOR_API_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s&text=%s&lang=%s";

    private final String apiKey;
    private final LanguageDetector languageDetector;

    public YandexTranslator(String apiKey, LanguageDetector languageDetector) {
        this.apiKey = apiKey;
        this.languageDetector = languageDetector;
    }

    @Override
    public Translation translate(TranslationRequest translationRequest) throws TranslatorException {
        try {
            URL url = new URL(prepareURL(translationRequest));
            HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
            JSONObject jsonObject = new JSONObject(IOUtils.toString(uc.getInputStream()));
            return new Translation(translationRequest.getText(), languageDetector.detect(translationRequest.getText()), jsonObject.get("text").toString(), translationRequest.getTargetLanguage());
        } catch (Exception ex) {
            throw new TranslatorException("Translation error");
        }
    }

    private String prepareURL(TranslationRequest translationRequest) throws Exception {
        return String.format(YANDEX_TRANSLATOR_API_URL, apiKey, translationRequest.getText(), prepareLanguageDirection(languageDetector.detect(translationRequest.getText()), translationRequest.getTargetLanguage()));
    }

    private String prepareLanguageDirection(Language from, Language to) {
        return from.getCode() + "-" + to.getCode();
    }
}
