package com.geekhub.sorter;

import static com.geekhub.sorter.Direction.ASC;
import static com.geekhub.sorter.Direction.DESC;

public class SelectionSorter implements ArraySorter {

    @Override
    public Comparable[] sort(Comparable[] elements, Direction direction) {
        for (int i = 0; i < elements.length; i++) {
            Comparable minValue = elements[i];
            int index = i;

            for (int j = i + 1; j < elements.length; j++) {
                if (elements[j].compareTo(minValue) < 0 && direction.equals(ASC)
                        || (elements[j].compareTo(minValue) > 0 && direction.equals(DESC))) {
                    minValue = elements[j];
                    index = j;
                }
            }
            if (i != index) {
                Comparable tmp = elements[i];
                elements[i] = elements[index];
                elements[index] = tmp;
            }
        }
        return elements;
    }
}
