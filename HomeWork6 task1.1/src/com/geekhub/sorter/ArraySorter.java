package com.geekhub.sorter;

public interface ArraySorter {

    Comparable[] sort (Comparable[] elements, Direction direction);

}
