package com.geekhub.sorter;

public enum Direction {

    ASC, DESC
}
