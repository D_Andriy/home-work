package com.geekhub.sorter;

import static com.geekhub.sorter.Direction.ASC;

public class InsertionSorter implements ArraySorter {

    @Override
    public Comparable[] sort(Comparable[] elements, Direction direction) {
        int j;
        Comparable key;
        int i;

        for (j = 1; j < elements.length; j++) {
            key = elements[j];
            if (direction.equals(ASC)) {
                for (i = j - 1; (i >= 0) && (elements[i].compareTo(key) > 0); i--) {
                    elements[i + 1] = elements[i];
                }
                elements[i + 1] = key;
            } else {
                for (i = j - 1; (i >= 0) && (elements[i].compareTo(key) < 0); i--) {
                    elements[i + 1] = elements[i];
                }
                elements[i + 1] = key;
            }
        }
        return elements;
    }
}
