package com.geekhub.sorter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<ArraySorter> sorters = new ArrayList<>();
        sorters.add(new BubbleSorter());
        sorters.add(new InsertionSorter());
        sorters.add(new SelectionSorter());
        for (ArraySorter sorter : sorters) {
            ComparableObject[] elements = {
                    new ComparableObject(1),
                    new ComparableObject(4),
                    new ComparableObject(7),
                    new ComparableObject(10),
                    new ComparableObject(3),
                    new ComparableObject(1),
                    new ComparableObject(6),
                    new ComparableObject(45),
                    new ComparableObject(23),
                    new ComparableObject(99),
                    new ComparableObject(567),
                    new ComparableObject(1),
                    new ComparableObject(0),
                    new ComparableObject(546)
            };
            System.out.println(Arrays.toString(sorter.sort(elements, Direction.ASC)));
            System.out.println(Arrays.toString(sorter.sort(elements, Direction.DESC)));
        }
    }
}
