package com.geekhub.sorter;

import static com.geekhub.sorter.Direction.ASC;
import static com.geekhub.sorter.Direction.DESC;

public class BubbleSorter implements ArraySorter {

    @Override
    public Comparable[] sort(Comparable[] elements, Direction direction) {

        for (int i = elements.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (elements[j].compareTo(elements[j + 1]) > 0 && direction.equals(ASC)
                        || (elements[j].compareTo(elements[j + 1]) < 0 && direction.equals(DESC))) {
                    Comparable tmp = elements[j];
                    elements[j] = elements[j + 1];
                    elements[j + 1] = tmp;
                }
            }
        }
        return elements;
    }
}
