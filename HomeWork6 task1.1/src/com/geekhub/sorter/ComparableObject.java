package com.geekhub.sorter;

public class ComparableObject implements Comparable {
    private int value;

    public ComparableObject(int value) {
        this.value = value;
    }

    @Override
    public int compareTo(Object o) {
        if (this.value == ((ComparableObject) o).value) {
            return 0;
        } else if (this.value > ((ComparableObject) o).value) {
            return 1;
        } else {
            return -1;
        }
    }

    @Override
    public String toString() {
        return "" + value;
    }
}
