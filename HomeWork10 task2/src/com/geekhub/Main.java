package com.geekhub;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static final String FILE_NAME = "D:\\test.txt";

    public static void main(String[] args) throws Exception {
        Generator generator = new Generator();
        int quantityThreads = 1;
        ExecutorService executor = Executors.newFixedThreadPool(quantityThreads);

        List<String> lines = Files.readAllLines(Paths.get(FILE_NAME), StandardCharsets.UTF_8);
        for (String link : lines) {
            Thread dataSaver = new DataWriter(generator.getMD5(link));
            executor.execute(dataSaver);

            quantityThreads++;
        }
        executor.shutdown();
    }
}
