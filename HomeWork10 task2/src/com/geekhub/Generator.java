package com.geekhub;

import java.io.InputStream;
import java.net.URL;
import java.security.MessageDigest;

public class Generator {

    public String getMD5(String link) {
        try {
            URL url = new URL(link);
            InputStream inputStream = url.openStream();
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            byte[] bytes = new byte[2048];
            int length;
            while ((length = inputStream.read(bytes)) != -1) {
                messageDigest.update(bytes, 0, length);
            }
            byte[] digest = messageDigest.digest();
            StringBuffer stringBuffer = new StringBuffer();
            String result = null;
            for (byte b : digest) {
                result = stringBuffer.append(String.format("%02x", b & 0xff)).toString();
            }

            return result;

        } catch (Exception e) {

        }
        return null;
    }
}