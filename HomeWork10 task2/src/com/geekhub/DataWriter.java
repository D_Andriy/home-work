package com.geekhub;

import java.io.FileWriter;

public class DataWriter extends Thread {

    public static final String FOLDER_FOR_SAVE = "D:\\md5.txt";
    private String result;

    public DataWriter(String result) {
        this.result = result;
    }

    @Override
    public void run() {

        try (FileWriter writer = new FileWriter(FOLDER_FOR_SAVE, true)) {
            writer.write(result);
            writer.flush();
            writer.append(System.getProperty("line.separator"));

        } catch (Exception e) {

        }
    }
}
