<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update user</title>
</head>
<body>
<form action="/updateUser" method="post">
    <input type="hidden" name="id" value="${user.id}">
    First name:<br>
    <input type="text" name="firstName" value="${user.firstName}">
    <p></p>
    Last name:<br>
    <input type="text" name="lastName" value="${user.lastName}">
    <p></p>
    Age:<br>
    <input type="text" name="age" value="${user.age}">
    <p></p>
    <input  type="submit" value="Save">
</form>
</body>
</html>
