<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table>
    <tr>
        <td>First name:</td>
        <td><c:out value="${user.firstName}"/></td>
    </tr>
    <tr>
        <td>Last name:</td>
        <td><c:out value="${user.lastName}"/></td>
    </tr>
    <tr>
        <td>Age:</td>
        <td><c:out value="${user.age}"/></td>
    </tr>
    <tr>
        <td><a href="/users">continue</a></td>

        <td><a href="/updateUser?edit&id=<c:out value="${user.id }"/>">edit</a></td>

        <td><a href="/deleteUser?id=<c:out value="${user.id }"/>">delete</a></td>

    </tr>
</table>
</body>
</html>
