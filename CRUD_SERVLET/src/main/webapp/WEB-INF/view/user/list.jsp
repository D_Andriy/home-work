<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<p>
    <a href="/createUser">Create new user</a>
</p>
<table border="1">
    <thead>
    <tr>
        <th>ID</th>
        <th>FIRST NAME</th>
        <th>LAST NAME</th>
        <th>AGE</th>
        <th colspan="3">ACTION</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${page.items}" var="user" varStatus="status">
        <tr>
            <td><c:out value="${user.id}"/></td>
            <td><c:out value="${user.firstName}"/></td>
            <td><c:out value="${user.lastName}"/></td>
            <td><c:out value="${user.age}"/></td>
            <td><a href="/showUser?&id=<c:out value="${user.id }"/>">show</a></td>
            <td><a href="/updateUser?&id=<c:out value="${user.id }"/>">edit</a></td>
            <td><a href="/deleteUser?&id=<c:out value="${user.id }"/>">delete</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<table>
    <tr>
        <td><a href="/users?page=1">first</a></td>
        <c:if test="${page.page != 1}">
            <td><a href="/users?page=${page.page - 1}">prev</a></td>
        </c:if>
        <c:forEach begin="1" end="${page.pageCount}" var="i">
            <c:choose>
                <c:when test="${page.page eq i}">
                    <td>...</td>
                </c:when>
                <c:otherwise>
                    <c:if test="${i < page.page + 4  && i > page.page - 4}">
                    <td><a href="/users?page=${i}">${i}</a></td>
                    </c:if>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${page.page lt page.pageCount}">
            <td><a href="/users?page=${page.page + 1}">next</a></td>
        </c:if>
        <td><a href="/users?page=${page.pageCount}">last</a></td>
    </tr>
</table>

</body>
</html>