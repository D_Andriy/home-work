package com.geekhub.userapp.web.listener;

import com.geekhub.userapp.repository.UserRepository;
import com.geekhub.userapp.repository.UserRepositoryImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();
        UserRepository userRepository = new UserRepositoryImpl();
        servletContext.setAttribute("userRepository", userRepository);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
