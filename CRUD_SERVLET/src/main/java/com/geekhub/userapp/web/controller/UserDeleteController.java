package com.geekhub.userapp.web.controller;

import com.geekhub.userapp.repository.UserRepository;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/deleteUser")
public class UserDeleteController extends HttpServlet {

    private UserRepository userRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        userRepository = (UserRepository) config.getServletContext().getAttribute("userRepository");
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        userRepository.delete(Integer.parseInt(id));
        response.sendRedirect("/users");
    }
}
