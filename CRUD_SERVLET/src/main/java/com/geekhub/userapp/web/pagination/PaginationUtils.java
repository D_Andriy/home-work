package com.geekhub.userapp.web.pagination;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class PaginationUtils {

    private PaginationUtils() {
    }

    public static <T> Page<T> getPage(List<T> items, PageRequest pageRequest) {
        int pageCount = (int) Math.ceil(items.size() * 1.0 / pageRequest.getPerPage());
        return new Page<>(
                pageRequest.getPage(),
                pageCount,
                items.stream()
                        .skip((pageRequest.getPage() - 1) * pageRequest.getPerPage())
                        .limit(pageRequest.getPerPage())
                        .collect(toList())
        );
    }
}
