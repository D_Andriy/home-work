package com.geekhub.source;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SourceLoader {

    @Autowired
    private List<SourceProvider> sourceProviders;

    public String loadSource(String pathToSource) throws Exception {
        for (SourceProvider sp : sourceProviders) {
            if (sp.isAllowed(pathToSource)) {
                return sp.load(pathToSource);
            } else {
                throw new Exception();
            }
        }

        return null;
    }
}
