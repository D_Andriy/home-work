package bean.comparator;

import java.lang.reflect.Field;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {
        A a = new A(120, "RX-8", "sedan", "red", 230);
        A b = new A(150, "RX-7", "coupe", "black", 230);
        compareFields(a, b);
    }

    private static void compareFields(Object a, Object b) throws IllegalAccessException {
        Class clazz = a.getClass();

        Field[] fieldsA = clazz.getDeclaredFields();
        for (Field f : fieldsA) {
            f.setAccessible(true);
            if (f.isAnnotationPresent(Ignore.class)) {continue;}

            if (f.get(a).equals(f.get(b))) {
                System.out.println(f.getName() + " " + true);
            } else {
                System.out.println(f.getName() + " " + false);
            }
        }
    }
}
