package bean.comparator;

public class A {

    int maxCount;
    String model;

    @Ignore
    String type;
    String color;
    int maxSpeed;

    public A(int maxCount, String model, String type, String color, int maxSpeed) {
        this.maxCount = maxCount;
        this.model = model;
        this.type = type;
        this.color = color;
        this.maxSpeed = maxSpeed;
    }
}
