package clone.creator;

import java.util.Arrays;
import java.util.List;

public class C {

    private List <Integer> list = Arrays.asList(1, 2, 3, 4);

    @Override
    public String toString() {
        return "C{" +
                "list=" + list +
                '}';
    }
}
