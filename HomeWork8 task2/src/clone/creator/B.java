package clone.creator;

public class B {

    private String color = "yellow";
    private String type = "B";

    @Override
    public String toString() {
        return "B{" +
                "color='" + color + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
