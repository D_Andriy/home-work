package clone.creator;

import java.lang.reflect.Field;

public class Main {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        A a = new A();
        Object clone = cloneObject(a);

        Class cloneClass = clone.getClass();
        Field[] fieldsClone = cloneClass.getDeclaredFields();
        for (Field f : fieldsClone) {
            f.setAccessible(true);
            System.out.println(f.getType() + " " + f.getName() + " " + f.get(clone) );
        }
    }

    private static Object cloneObject(Object o) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Object clone;
        Class clazz = o.getClass();
        clone = clazz.newInstance();
        Field[] fields = clazz.getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            f.set(clone, f.get(o));
        }

        return clone;
    }
}
