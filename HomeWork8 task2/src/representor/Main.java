package representor;

import java.lang.reflect.Field;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {
        Cat cat = new Cat();
        Human human = new Human();
        Car car = new Car();
        reflection(car);
        reflection(human);
        reflection(cat);
    }

    private static void reflection(Object o) throws IllegalAccessException {
        Class clazz = o.getClass();
        System.out.println(clazz.getName());

        Field[] fields = clazz.getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);

            if (f.isAnnotationPresent(Ignore.class)) {
                continue;
            }

            System.out.println(f.getName() + " " + f.get(o));
        }
    }
}
