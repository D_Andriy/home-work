package com.geekhub.shape;

public interface Shape {

    double calculateArea();

    double calculatePerimeter();
}
