package com.geekhub.json.adapter;

import java.awt.Color;

public class ColorAdapter implements JsonDataAdapter<Color> {

    @Override
    public Object toJson(Color o) {
        return "(" + o.getRed() + "," + o.getGreen() + "," + o.getBlue() + ")";
    }
}
