package com.geekhub.json.adapter;

import com.geekhub.json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Collection;

public class CollectionAdapter implements JsonDataAdapter<Collection> {

    @Override
    public Object toJson(Collection c) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (Object col : c) {
            jsonArray.put(JsonSerializer.serialize(col));
        }
        return jsonArray;
    }
}
