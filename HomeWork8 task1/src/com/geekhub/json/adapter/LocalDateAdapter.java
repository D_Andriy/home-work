package com.geekhub.json.adapter;

import java.time.LocalDate;

public class LocalDateAdapter implements JsonDataAdapter<LocalDate> {

    @Override
    public Object toJson(LocalDate date) {
       String text = date.toString();

        return text;
    }
}
