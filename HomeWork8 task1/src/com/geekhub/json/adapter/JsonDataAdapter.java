package com.geekhub.json.adapter;

import org.json.JSONException;

public interface JsonDataAdapter<T> {

    Object toJson(T o) throws JSONException;
}
