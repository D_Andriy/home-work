package com.geekhub.json;

import com.geekhub.json.adapter.JsonDataAdapter;
import com.geekhub.json.adapter.UseDataAdapter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class JsonSerializer {

    private static Set<Class> simpleTypes = new HashSet<Class>(Arrays.asList(
            JSONObject.class,
            JSONArray.class,
            String.class,
            Integer.class,
            Short.class,
            Long.class,
            Byte.class,
            Double.class,
            Float.class,
            Character.class,
            Boolean.class,
            int.class,
            short.class,
            long.class,
            byte.class,
            double.class,
            float.class,
            char.class,
            boolean.class
    ));

    public static Object serialize(Object o) {
        if (null == o) {
            return "null";
        }
        if (simpleTypes.contains(o.getClass())) {
            return o;
        } else {
            try {
                return toJsonObject(o);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private static JSONObject toJsonObject(Object o) throws Exception {
        JSONObject results = new JSONObject();
        Class clazz = o.getClass();
        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);

            if (field.isAnnotationPresent(Ignore.class)) {
                continue;
            }
            if (field.isAnnotationPresent(UseDataAdapter.class)) {
                JsonDataAdapter adapter = field.getAnnotation(UseDataAdapter.class).value().newInstance();
                results.put(field.getName(), adapter.toJson(field.get(o)));
            } else {
                results.put(field.getName(), field.get(o));
            }
        }

        return results;
    }
}
