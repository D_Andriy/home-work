function loadComments() {
    $(document).ready(function () {
        $.get("/comments", function (data) {
            var content = '';
            $.each(data, function (i, comment) {
                content +=( "<tr><td>" + comment.date + "</td>" +
                    "<td>" + comment.text + "</td></tr>");
            });
            $("#content").html(content)
        });
    });
}

function saveComment() {
    var comment = $("input[name=text]").val();
    $.ajax({
        url: "comments",
        type: "POST",
        data: {comment: comment}
    });
}