package com.geekhub.comments;

import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CommentRepository {

    private List<Comment> comments = new ArrayList<>();

    public void save(Comment comment) {
        comments.add(comment);
    }

    public List<Comment> find(int count) {
        return comments.stream()
                .sorted(Comparator.comparing(Comment::getDate).reversed())
                .limit(count)
                .collect(Collectors.toList());
    }
}
