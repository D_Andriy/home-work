package com.geekhub.config;

import com.geekhub.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class AppConfig {

    @Bean
    public Car car(Wheel wheel, Engine engine) {
        List<Wheel> wheels = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            wheels.add(wheel);
        }
        return new Car(engine, wheels);
    }

    @Bean
    public Engine engine() {
        return new Engine(2.5);
    }

    @Bean
    public Wheel wheel(Tyres tyres) {
        return new Wheel(tyres);
    }

    @Bean
    public Tyres tyres() {
        return new SummerTyres(200, "TOYO");
    }

}
