package com.geekhub.shape;

import org.junit.Test;

import static com.geekhub.shape.dataprovider.ShapeDataProvider.getCircle;
import static org.junit.Assert.assertEquals;

public class ShapeFactoryTest {

    final String CIRCLE = "Circle";
    ShapeFactory shapeFactory = new ShapeFactory();

    @Test
    public void shouldCorrectGetShape() throws Exception {
        double[] params = {45};
        assertEquals(getCircle(), shapeFactory.getShape(CIRCLE, params));
    }
}