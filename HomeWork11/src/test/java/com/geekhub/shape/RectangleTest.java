package com.geekhub.shape;

import org.junit.Test;

import static com.geekhub.shape.dataprovider.ShapeDataProvider.getRectangle;
import static org.junit.Assert.assertEquals;

public class RectangleTest {

    @Test
    public void shouldCorrectCalculateArea() throws Exception {
        assertEquals(1806, getRectangle().calculateArea(), 0.01);
    }

    @Test
    public void shouldCorrectCalculatePerimeter() throws Exception {
        assertEquals(170, getRectangle().calculatePerimeter(), 0.01);
    }

    @Test
    public void shouldCorrectCalculateTriangleArea() throws Exception {
        assertEquals(903, getRectangle().triangleArea(), 0.01);
    }

    @Test
    public void shouldCorrectCalculateTrianglePerimeter() throws Exception {
        assertEquals("trianglePerimeter", 145.10, getRectangle().trianglePerimeter(), 0.01);
    }
}