package com.geekhub.shape.dataprovider;

import com.geekhub.shape.Circle;
import com.geekhub.shape.Rectangle;

public class ShapeDataProvider {

    public static Circle getCircle() {
        return new Circle(45);
    }

    public static Rectangle getRectangle() {
        return new Rectangle(42, 43);
    }
}
