package com.geekhub.shape;

import org.junit.Test;

import static com.geekhub.shape.dataprovider.ShapeDataProvider.getCircle;
import static org.junit.Assert.*;

public class CircleTest {

    @Test
    public void shouldCorrectCalculateArea() throws Exception {
        assertEquals(6361.72, getCircle().calculateArea(), 0.01);
    }

    @Test
    public void shouldCorrectCalculatePerimeter() throws Exception {
        assertEquals(282.74, getCircle().calculatePerimeter(), 0.01);
    }
}