package com.geekhub.linkedlist;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedListImplTest {

    @Test
    public void shouldAddElementToLinkedList() {
        LinkedList<Element> linkedList = new LinkedListImpl<>();
        assertTrue(linkedList.isEmpty());
        linkedList.add(new Element(0, "O"));

        assertEquals(1, linkedList.size());
    }

    @Test
    public void shouldAddElementsEvenTheyAreEquals() {
        LinkedList<Element> linkedList = new LinkedListImpl<>();
        linkedList.add(new Element(0, "O"));
        linkedList.add(new Element(0, "O"));

        assertEquals(2, linkedList.size());
    }

    @Test
    public void shouldSaveCorrectOrderForElement() {
        LinkedList<Element> linkedList = new LinkedListImpl<>();

        linkedList.add(new Element(0, "O"));
        linkedList.add(new Element(1, "A"));
        linkedList.add(new Element(2, "B"));
        linkedList.add(new Element(3, "C"));
        linkedList.add(new Element(4, "D"));

        assertEquals(new Element(0, "O"), linkedList.get(0));
        assertEquals(new Element(1, "A"), linkedList.get(1));
        assertEquals(new Element(2, "B"), linkedList.get(2));
        assertEquals(new Element(3, "C"), linkedList.get(3));
        assertEquals(new Element(4, "D"), linkedList.get(4));
    }

    @Test(expected = NullPointerException.class)
    public void shouldFailWhenElementNotExistForCurrentOrder() {
        LinkedList<Element> linkedList = new LinkedListImpl<>();
        linkedList.add(new Element(0, "O"));

        assertEquals(new Element(0, "O"), linkedList.get(0));
        linkedList.get(1);
    }

    @Test
    public void shouldDeleteElementByIndex() {
        LinkedList<Element> linkedList = new LinkedListImpl<>();

        linkedList.add(new Element(0, "O"));
        linkedList.add(new Element(1, "A"));

        Element deletedElement = linkedList.delete(0);
        assertEquals(new Element(0, "O"), deletedElement);
        assertEquals(1, linkedList.size());
        assertFalse(linkedList.contains(deletedElement));
        assertTrue(linkedList.contains(new Element(1, "A")));
    }

    @Test
    public void shouldDeleteElementAndShiftOtherElements() {
        LinkedList<Element> linkedList = new LinkedListImpl<>();

        linkedList.add(new Element(0, "O"));
        linkedList.add(new Element(1, "A"));
        linkedList.add(new Element(2, "B"));

        Element deletedElement = linkedList.delete(1);
        assertEquals(new Element(1, "A"), deletedElement);
        assertFalse(linkedList.contains(deletedElement));
        assertEquals(new Element(0, "O"), linkedList.get(0));
        assertEquals(new Element(2, "B"), linkedList.get(1));
    }

    @Test
    public void shouldDeleteElement() {
        LinkedList<Element> linkedList = new LinkedListImpl<>();

        linkedList.add(new Element(0, "O"));
        linkedList.add(new Element(1, "A"));

        Element elementToDelete = new Element(0, "O");
        assertTrue(linkedList.delete(elementToDelete));
        assertEquals(1, linkedList.size());
        assertFalse(linkedList.contains(elementToDelete));
        assertTrue(linkedList.contains(new Element(1, "A")));
    }

    @Test
    public void shouldCleanListAndReturnQuantityElements() {
        LinkedList<Element> linkedList = new LinkedListImpl<>();

        linkedList.add(new Element(0, "O"));
        linkedList.add(new Element(1, "A"));

        assertEquals(2, linkedList.size());
        assertEquals(2, linkedList.clean());
        assertTrue(linkedList.isEmpty());
    }
}