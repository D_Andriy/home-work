package com.geekhub.translator;

import com.geekhub.translator.core.Translation;
import com.geekhub.translator.core.TranslationRequest;
import com.geekhub.translator.core.Translator;
import com.geekhub.translator.core.TranslatorException;
import com.geekhub.translator.core.language.Language;
import com.geekhub.translator.core.language.LanguageDetector;
import org.junit.Test;

import static com.geekhub.translator.TranslatorController.YANDEX_API_KEY;
import static com.geekhub.translator.core.language.Language.ENGLISH;
import static com.geekhub.translator.core.language.Language.UKRAINIAN;
import static org.junit.Assert.assertEquals;

public class TranslatorControllerTest {

    @Test
    public void shouldCorrectTranslateFromEnglishToUkrainian() throws Exception {
        LanguageDetector languageDetector = new YandexLanguageDetector(YANDEX_API_KEY);
        Translator translator = new YandexTranslator(YANDEX_API_KEY, languageDetector);
        TranslationRequest translationRequest = new TranslationRequest("dog", Language.find("uk"));
        Translation translation = translator.translate(translationRequest);

        assertEquals("dog", translation.getOriginalText());
        assertEquals(ENGLISH, translation.getOriginalLanguage());
        assertEquals("[\"собака\"]", translation.getTranslatedText());
        assertEquals(UKRAINIAN, translation.getTargetLanguage());
    }

    @Test
    public void shouldCorrectTranslateFromUkrainianToEnglish() throws Exception {
        LanguageDetector languageDetector = new YandexLanguageDetector(YANDEX_API_KEY);
        Translator translator = new YandexTranslator(YANDEX_API_KEY, languageDetector);
        TranslationRequest translationRequest = new TranslationRequest("кіт", Language.find("en"));
        Translation translation = translator.translate(translationRequest);

        assertEquals("кіт", translation.getOriginalText());
        assertEquals(UKRAINIAN, translation.getOriginalLanguage());
        assertEquals("[\"cat\"]", translation.getTranslatedText());
        assertEquals(ENGLISH, translation.getTargetLanguage());
    }

    @Test(expected = TranslatorException.class)
    public void shouldFailIfYandexApiKeyIsNotCorrect() throws Exception {
        LanguageDetector languageDetector = new YandexLanguageDetector("123");
        Translator translator = new YandexTranslator("123", languageDetector);
        TranslationRequest translationRequest = new TranslationRequest("lol", Language.find("uk"));
        translator.translate(translationRequest);
    }

    @Test(expected = TranslatorException.class)
    public void shouldFailIfLanguageForWordIsNotDetected() throws Exception {
        LanguageDetector languageDetector = new YandexLanguageDetector(YANDEX_API_KEY);
        Translator translator = new YandexTranslator(YANDEX_API_KEY, languageDetector);
        TranslationRequest translationRequest = new TranslationRequest("dfhdfhfgjdj", Language.find("uk"));
        translator.translate(translationRequest);
    }
}
