import java.io.IOException;

public class Test {
    public static void main(String[] args) {
        try {
            testString(new String(""));
            testBuffer(new StringBuffer(""));
            testBuilder(new StringBuilder(""));
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void testString(String obj) throws IOException {
        long before = System.currentTimeMillis();
        for (int i = 0; i++ < 1e9; ) {
            obj.concat("");
        }
        long after = System.currentTimeMillis();
        System.out.println(obj.getClass()
                .getSimpleName() + ": " + (after - before) / 1000 + "s.");
    }

    private static void testBuffer(StringBuffer obj) throws IOException {
        long before = System.currentTimeMillis();
        for (int i = 0; i++ < 1e9; ) {
            obj.append("");
        }
        long after = System.currentTimeMillis();
        System.out.println(obj.getClass()
                .getSimpleName() + ": " + (after - before) / 1000 + "s.");
    }

    private static void testBuilder(StringBuilder obj) throws IOException {
        long before = System.currentTimeMillis();
        for (int i = 0; i++ < 1e9; ) {
            obj.append("");
        }
        long after = System.currentTimeMillis();
        System.out.println(obj.getClass()
                .getSimpleName() + ": " + (after - before) * 1000 + "s.");
    }
}
